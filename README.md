My approach is to start from an opinionated distro (Garuda) then customize post-install.

Gnu Stow creates symbolic links from the current worktree to the home directory incentivize consistency and upkeep across workstations.

## Stow

This repo is essentially a collection of stow packages, most of which target the home directory:

1. Clone the repo in the source directory
2. Restore in the target directory: `stow -t ~ emacs`

Check the content of each package first, if it contains a well-known root directory like `etc`, then target the systems root.

## Packages

I compile packages in text files to feed `paru`.

``` shell
paru -S --needed - <packages/terminal.txt
```

There's no version locking. This is just an easy way to remember what to install. There may be issues to resolve with `paru` prompts. 

By convention, any stow package may include a `.packages` for its dependencies. In Addition, the `packages` package contains more general package groups and scripts to easily install them.

``` shell
❯ install-packages develop.txt
Installing ~/.packages/develop.txt
[sudo] password for user:
...
```

Uninstall unwanted default packages.

``` shell
❯ remove-packages bloat.remove
```

### Upgrade

Use the Garuda script to minimize odds of breaking something.

The `-a` switch includes AUR packages.

``` shell
update -a
```

## Patch Files

Consider using patch files to keep small changes to large configs to avoid carrying older configurations.

## Filesystem

### BTRFS

I'm experimenting with BTRFS since it's the default in Garuda. The snapshotting feature can be tricky and has filled my disk before. Basic usage on a system disk has not caused any problems, even with luks encryption. Its toolchain is user friendly and can make raid-like volumes in one place. I wouldn't use it on a server yet until I find it easier that ext4 and raid0.

- Create volumes for write heavy things like qcows files to exclude from scheduled snapshots. For example, volume / snapshotted by default does not include hypothetical volume `/var/lib/libvirt/images`.
- Edit `/etc/snapper/configs/root` if system disk space is an issue, see: https://syang.io/2017/01/16/Fix-Space-Problem.html Be aware that snapshots can take half the disk by default: `SPACE_LIMIT="0.5"`. I've seen this happen with large qcow2 files in the root subvolume (I think because I defragged from the guess). 

## Doom Emacs

Doom Emacs is another opinionated framework that auto-configures my programming environment.

## Brave Browser

Join the existing Brave sync chain to setup the browser including settings and bookmarks.

## Libvirt

Followed this tutorial for virt-manager access via ssh: https://computingforgeeks.com/install-kvm-qemu-virt-manager-arch-manjar/

### Resize Disk

Tested example with Ubuntu 20.04 guest:
``` shell
$ sudo rsync -aS --progress ./eth2.qcow2 /mnt/media/Images/
$ sudo qemu-img resize eth2.qcow2 +500G
$ virt-filesystems --logical-volumes --long -a /mnt/media/Images/eth2.qcow2
Name                      Type  Size           Parent
/dev/ubuntu-vg/ubuntu-lv  lv    1097359949824  /dev/ubuntu-vg
sudo virt-resize --expand /dev/sda3 --LV-expand /dev/ubuntu-vg/ubuntu-lv /mnt/media/Images/eth2.qcow2 ./eth2.qcow2
```

### Filesharing

Use virtiofs to [mount a volume on the guest](https://libvirt.org/kbase/virtiofs.html#sharing-a-host-directory-with-a-guest)

By convention, store the volume data in `~/Virt/<guest name>` and call the device `tmp`.

Works best if both the host an guest directory is owned by `1000:1000`.

## Network

Garuda installs the NetworkManager systemd unit.

Use `ip` and `iw` for everything, starting with `ip -4 address` and `iw dev`.

Use `aircrack-ng` to find Internet starting with: https://trendoceans.com/start-and-stop-monitor-mode-in-linux/

## GPG

Store SSH keys in Yubikey cards: https://github.com/drduh/YubiKey-Guide#replace-agents

### Thinkpad 

I created a package list for each model (e.g. `packages/x1-extreme.txt`) with the packages I actually installed.

Procedures I followed include:

1. [Configure thermald to use dptf tables](https://wiki.archlinux.org/title/Lenovo_ThinkPad_X1_Extreme#Configuring_thermald_to_use_extracted_dptf_tables)
2. [Install optimus-manager](https://wiki.archlinux.org/title/NVIDIA_Optimus#Using_optimus-manager)
3. [Test SGX](https://github.com/ayeks/SGX-hardware#test-sgx) 

## Maintenance

The Arch Wiki serves as the primary operating manual. What I'm looking for in the Garuda distro is
tooling to simplify maintenance. Specifically, auto-fix breaking changes. 

Garuda tooling usage so far has been limited to:

1. `garuda-update` instead of `pacman -Syu` hoping that it will attempt to resolve compatibility issues.
2. `garuda-boot-options` to install grup on the boot partition.
4. `garuda-assistant` as needed to generate factory configs.
5. Planning to make use of BTRFS snapshots when in need.
