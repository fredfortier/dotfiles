#!/bin/bash

RES=`pactl get-source-mute @DEFAULT_SOURCE@`
ICON="mic_on"
STATE="Warning"

if [[ $RES == *"yes"* ]]; then
    ICON="mic_off"
    STATE="Good"
fi


echo "{\"icon\":\"${ICON}\",\"state\":\"${STATE}\",\"text\":\"\"}"
