#!/bin/bash

# Configure in ~/.personal
source ~/.personal
# IDS=("bitcoin" "ethereum")
# SYMBOLS=("BTC" "ETH")

# Separate the ids for the URL as Coingecko's example:
# `https://api.coingecko.com/api/v3/simple/price?ids=bitcoin%2Cethereum&vs_currencies=usd`
DELIM="%%2C"
PARAM=$(printf "${DELIM}%s" "${IDS[@]}")
PARAM=${PARAM:3}

RES=$(curl -s -X 'GET' \
  "https://api.coingecko.com/api/v3/simple/price?ids=${PARAM}&vs_currencies=usd&include_24hr_change=true" \
  -H 'accept: application/json' \
  | jq -r 'to_entries[] | [.key, (.value.usd|.*100|round|./100|tostring), "+"+(.value.usd_24h_change|.*10|round|./10|tostring)+"%"] | join(",")' \
  | xargs)

for i in "${!IDS[@]}"; do
    RES=$(echo "$RES" | sed -e "s/${IDS[$i]},/${SYMBOLS[$i]},\$/g")
done
echo "$RES" | sed  -e "s/,/ /g" -e "s/+-/-/g"
