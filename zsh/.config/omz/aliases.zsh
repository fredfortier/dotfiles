# Network
alias ipinfo='curl -s https://ipinfo.io | jq -r ".ip"'
alias ports-open="sudo lsof -i -P -n"
alias ports-scan='function _(){ nc -z -v "$1" "$2" 2>&1 |rg -v "Connection refused"; }; _'
alias iperf-server='iperf3 -s -p 5201'
alias iperf-client='function _(){ iperf3 -c "$1" -p 5201; }; _'
# Clipboard
alias clip='xclip -selection clipboard'
# alias clip-fd='function _(){ fd "$1" |tail -n1 |clip }; _'
# alias clip-path='function _(){ realpath "$1" |clip }; _'
# Files
alias enclose='function _(){ mkdir "$(basename "${1%.*}")"; mv "$1" "$(basename "${1%.*}")"/; }; _'
# Cracked software
alias pull-norar='function _(){ rsync -avP --exclude "*.r*" "$1" "$2"; }; _'
alias fcd="cd \$(fd --type d --max-depth 4 | fzf)"
alias makeiso='mkisofs -R -J -joliet-long -jcharset UTF-8'
alias mouseless-up="sudo mouseless --config=/home/user/.config/mouseless/config.yaml"
# alias vim=nvim
alias vimiv='vimiv -o "%m"'
# Run emacs in the terminal
alias emacs='emacs -nw'

# TODO: Prefer the oh-my-zsh aliases when good enough
# Tiny aliases for most frequently used docker commands
# Inspired by: https://gist.github.com/jgrodziski/9ed4a17709baad10dbcd4530b60dfcbb
alias dc='docker compose'
# Returns the first Docker container name matching the given pattern.
alias dcc='function _(){ docker compose ps 2>/dev/null | rg "^([A-Za-z0-9\-]*_[A-Za-z0-9\-]*?$1[A-Za-z0-9\-]*?_1)\s*.*$" -r '"'"'$1'"'"' | tail -n1; }; _'
alias dcl='function _(){ docker compose logs $(dcs "$1") ${@:2} 2>&1; }; _'
alias dclf='function _(){ dcl "$1" --tail 20 -f ${@:2}; }; _'
# If the vector role is installed, this will transform the JSON logs into a more readable format.
alias dcv='function _(){ dcl "$1" ${@:2} | vector -q; }; _'
alias dcvf='function _(){ dcv "$1" --tail 20 -f ${@:2}; }; _'
alias dcd='docker compose down --remove-orphans'
# Docker daemon commands using the docker compose context for fuzzy container name matching.
alias dl='function _(){ docker logs $(dcc "$1") ${@:2}; }; _'
alias dlf='function _(){ dl "$1" --tail 20 -f ${@:2}; }; _'
alias da='function _(){ docker attach $(dcc "$1") ${@:2}; }; _'

# Containerized tools
alias analyzer='docker ps --format=json |rg analyzer |jq -r ".ID" |xargs -I{} docker exec {}'

# Gitlab
alias glab-todos='glab issue list --assignee @me --label "TODO::Update" -Fids --per-page 1000'
alias glab-design-proposed='glab issue list --label "Design::Proposed" -Fids --per-page 1000'
alias glab-mine='glab issue list --assignee @me -Fids --per-page 1000'
alias glab-mine-nobacklog='glab-mine | xargs -l bash -c '"'"'if glab issue view $0 | rg -v "milestone.*Backlog" | rg -q milestone; then echo $0; else echo $0; fi'"'"
alias glab-view='xargs -l glab issue view --web'

# Math and crypto stuff
alias token-units='function _(){ echo "obase=16; $1 * 10^18" | bc | xargs printf "%64s\n" | rg "\s" -r "0" | tr "[:upper:]" "[:lower:]"; }; _'

# Cloud stuff
alias vast-search-3090='function _(){ vast search offers "reliability > 0.98 num_gpus=${1:-1} gpu_name=RTX_3090"; }; _'

# Replace yay with paru if installed
[ ! -x /usr/bin/yay ] && [ -x /usr/bin/paru ] && alias yay='paru'

# GPG helpers
gpg-encrypt () {
        output=~/"${1}".$(date +%s).enc
        gpg --encrypt --armor --output ${output} -r 0x0000 -r 0x0001 "${1}" && echo "${1} -> ${output}"
}
gpg-decrypt () {
        output=$(echo "${1}" | rev | cut -c16- | rev)
        gpg --decrypt --output ${output} "${1}" && echo "${1} -> ${output}"
}
