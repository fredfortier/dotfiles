# Don't share history between sessions
setopt no_share_history

# ============== Path Config ==============
if [ -d "$HOME/.local/bin" ]; then
    export PATH=$HOME/.local/bin:$PATH
fi
export PATH="$HOME/.cargo/bin:$HOME/.vector/bin:$HOME/.emacs.d/bin:$HOME/.config/emacs/bin:$HOME/.deno/bin:$PATH"
# See: https://wiki.archlinux.org/title/Systemd/User#PATH
systemctl --user import-environment PATH
systemctl --user set-environment XDG_CONFIG_HOME="$HOME/.config"
systemctl --user set-environment XDG_STATE_HOME="$HOME/.local/state"
systemctl --user set-environment XDG_DATA_HOME="$HOME/.local/share"
# ============================================

# ============== Emacs Related ==============
# Important because emacs tramp sets this variable to 'dumb', it expects a simple prompt or will freeze.
# See details: https://archive.ph/HYNn7
if [[ $TERM == "dumb" ]]; then
   unsetopt zle
   PS1='$ '
   return
fi
# Emacs colors see: https://github.com/syl20bnr/spacemacs/wiki/Terminal
export COLORTERM=truecolor
# This should be a factory settings, not sure why I had to set it.
# See: https://stackoverflow.com/questions/6804208/nano-error-error-opening-terminal-xterm-256color
export TERMINFO=/usr/share/terminfo
# ============================================

# Prefer whatever release of brave in installed
if which brave &> /dev/null; then
  export BROWSER=brave
elif which pacman &> /dev/null; then
  export BROWSER=$(pacman -Q | rg '^(chromium)|(firefox)(-\w+).*$' -r '$1$2' || echo 'firefox' | head -n1)
fi

if [ -f "$(dirname $0)"/local.zsh ]; then
    source "$(dirname $0)"/local.zsh
fi
