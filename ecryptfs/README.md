Here's how I installed ecryptfs-simple on Ubuntu. On arch, it's in the AUR.

``` shell
git clone https://github.com/mhogomchungu/ecryptfs-simple.git
sudo apt install libecryptfs-dev
sudo apt install libgcrypt-dev
sudo apt install libmount-dev
cd ecryptfs-simple/
mkdir build ; cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=release ..
make
sudo make install
ecryptfs-simple --help
```

