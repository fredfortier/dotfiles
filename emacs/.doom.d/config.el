;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Frederic Fortier"
      user-mail-address "ffortier@dexlabs.xyz")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; TODO: Had to change size to 18 to work on the laptop display
;; (setq doom-font (font-spec :family "DejaVu Sans Mono" :size 15)
;;       doom-variable-pitch-font (font-spec :family "DejaVu Sans Mono")
;;       doom-big-font (font-spec :family "DejaVu Sans Mono" :size 18))
(setq doom-font (font-spec :family "Noto Sans Mono" :size 17)
      doom-variable-pitch-font (font-spec :family "Noto Sans" :size 17)
      doom-big-font (font-spec :family "Noto Sans" :size 18))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
(setq doom-theme 'doom-dracula)

;; Buffer encoding
(setq-default buffer-file-coding-system 'utf-8-unix)
;; dir-locals.el overwrites https://www.reddit.com/r/emacs/comments/nzur7x/emacs_doesnt_ask_me_to_mark_dirlocal_variables_as/
;;(setq-default enable-local-variables t)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

;; Platformio: https://github.com/ZachMassia/platformio-mode
;; Enable ccls for all c++ files, and platformio-mode only
;; when needed (platformio.ini present in project root).
;; (require 'platformio-mode)
;; (add-hook 'c++-mode-hook (lambda ()
;;                           (lsp-deferred)
;;                           (platformio-conditionally-enable)))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Media/Shared/Org/")
(setq org-agenda-files (list "Inbox.org" "Journal.org" "Projects.org" "Notes.org"))

;; Org roam
;;(setq org-roam-directory (file-truename (concat org-directory "roam")))
;;(org-roam-db-autosync-mode)

(require 'org-habit)
;; See: https://github.com/hlissner/doom-emacs/issues/2913
(after! org
  (setq org-use-tag-inheritance t)
  ;; TODO keywords
  (setq org-todo-keywords
        '((sequence "TODO(t)" "NOTE(n)" "ISSUE(i)" "ENTRY(e)" "IDEA(j)" "|" "PENDING(p)" "DONE(d)" "DELEGATED(g)" "CANCELLED(c)")))

  ;; Capture templates
  (setq org-capture-templates
        '(("i" "Inbox" entry (file "Inbox.org")
           "* TODO %?\n")
          ("j" "Journal" entry (file+datetree "Journal.org")
           "* ENTRY %?\nEntered on %U\n")))

  ;; Agenda formatting
  (setq org-agenda-hide-tags-regexp ".")
  (setq org-agenda-prefix-format
        '((agenda . " %i %-12:c%?-12t% s")
          (todo   . " %i %-12:c")
          (tags   . " %i %-12:c")
          (search . " %i %-12:c")))

  ;; Refile
  (setq org-refile-targets
        '(("Projects.org" :level . 1)
          ("Notes.org" :level . 1)
          ("Journal.org" :level . 1)))
  )

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

(setq python-shell-exec-path "/opt/anaconda/bin")

;; LSP
;; TODO: Find a way to run `nvm use' in .dir-locals
;; (setq exec-path (append exec-path '("~/.nvm/versions/node/v12.22.7/bin")))
;; (setq lsp-clients-typescript-tls-path '("~/.nvm/versions/node/v12.22.7/bin"))

(setq lsp-diagnostics-flycheck-default-level 'warning)
(setq lsp-idle-delay 0.6)
(setq lsp-ui-imenu-window-width 50)
(setq lsp-enable-file-watchers t)
;; I don't have a sense of how much is too much. 1000 seemed too low and resulted in constant warning.
(setq lsp-file-watch-threshold 20000)

(setq lsp-rust-analyzer-server-command '("ddx-lsp.sh" "rust-analyzer"))
(use-package! rustic
  :config (setq rustic-analyzer-command '("ddx-lsp.sh" "rust-analyzer"))
  )
;; (setq lsp-pyright-python-executable-cmd "ddx-lsp.sh pyright")


;; (setq rustic-cargo-bin "cargo-split.bash")
;; Rust analyzer specific inlay hints ignored? https://github.com/doomemacs/doomemacs/issues/7380#issuecomment-1931916915
(setq lsp-rust-analyzer-server-format-inlay-hints t)
(setq lsp-rust-analyzer-display-parameter-hints t)
(setq lsp-rust-analyzer-cargo-watch-enable t)
(setq lsp-inlay-hint-enable t)
(setq lsp-rust-clippy-preference "off")
;; (setq lsp-rust-analyzer-cargo-watch-command "check")
(setq lsp-rust-all-targets t)
;; Disable if any project uses the conditional re-export pattern driven by the SGX SDK.
(setq lsp-rust-all-features nil)
(setq lsp-rust-analyzer-proc-macro-enable t)
(setq lsp-rust-unstable-features t)
;; Got multiple false positives when attempted.
(setq lsp-rust-analyzer-diagnostics-enable-experimental nil)
;; By convention, the develop feature includes common dependencies for unit and integration tests (e.g. fixtures).
;;(setq lsp-rust-features ["develop"])
;; Keep this off for SGX SDK programs to avoid overhead leading to instability.
(setq lsp-rust-analyzer-cargo-run-build-scripts nil)
(setq lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")


(setq rustic-format-trigger 'on-save)
(add-hook! 'rustic-mode-hook (modify-syntax-entry ?_ "w"))
(add-hook! 'rustic-mode-hook 'flyspell-prog-mode)

;; Logs
(add-hook! 'typescript-mode-hook 'prettier-mode)
(setq-hook! 'typescript-mode-hook +format-with 'prettier-prettify)
(add-hook! 'typescript-tsx-mode-hook 'prettier-mode)
(setq-hook! 'typescript-tsx-mode-hook +format-with 'prettier-prettify)

;; Editor
(setq evil-snipe-scope 'buffer)
(setq-default fill-column 100)

;; Enables fast paste (xP) and similar patterns: https://emacs.stackexchange.com/a/53536
(define-key evil-normal-state-map "x" 'delete-forward-char)
(define-key evil-normal-state-map "X" 'delete-backward-char)

;; TODO: Temp worakround: https://github.com/hlissner/doom-emacs/issues/4070
(setq enable-local-variables t)

;; Ripgrep
(rg-enable-menu)
(setq rg-hide-command nil)
(setq rg-custom-type-aliases
      '(("env" .    "*.env .env") ("sol" .    "*.sol")
        ))


;; Key Bindings
(map! :leader
      :desc "Ripgrep"
      "/" #'rg)

(map! :leader
      :desc "Run here"
      "!" #'shell-command)

(map! :leader
      :desc "Open LSP imenu"
      "o i" #'lsp-ui-imenu)

(map! :leader
      :desc "Check syntax"
      "b f" #'flycheck-buffer)

(map! :leader
      :desc "Ediff with revision"
      "g e" #'ediff-revision)

(map! :leader
      :desc "Correct word"
      "c w" #'flyspell-correct-at-point)

(map! :leader
      "c W" nil)

(map! :leader
      "w 2" #'(evil-window-set-height 20))

;; Ctrl-Shift shortcuts
(map! "C-S-p" #'projectile-switch-project)

(map! :leader
      :desc "Capture web link from clipboard"
      :mode 'evil-org
      "m w" #'org-cliplink)

(defun ai/edit-note ()
  "Use AI tooling to edit a note file defined by convention based on the project context."
  (interactive)
  (projectile-save-project-buffers)
  (projectile-with-default-dir (projectile-acquire-root)
    ;; TODO: Choose text file based on project context
    ;; If project directory is "derivadex" run with the "ddx/writer" subcommand, else use "personal/writer".
    (shell-command "ai-run ddx/writer edit" "*ai*" "*ai*")))
;; Mirror the project in context of the buffer
(map! :leader
      :desc "Edit project node with AI"
      "p n" #'ai/edit-note)


;; ### BEGIN: Project specific settings ###
;; Pushing project mirror to remote filesystem
(defun mirror ()
  "Invoke mirror.bash from the project root."
  (interactive)
  (projectile-save-project-buffers)
  (projectile-with-default-dir (projectile-acquire-root)
    (shell-command (concat "./mirror.bash" ) "*mirror*" "*mirror*")))
;; Mirror the project in context of the buffer
(evil-ex-define-cmd "m[irror]" 'mirror)
(map! :leader
      :desc "Mirror project"
      "p m" #'mirror)
;; Targets remote workstation
(defun mirror-op ()
  (interactive)
  (projectile-save-project-buffers)
  (projectile-with-default-dir (projectile-acquire-root)
    (shell-command "DDX_MIRROR_DIR=op5:/usr/local/src/ff1 ./mirror.bash" "*mirror*" "*mirror*")))
(evil-ex-define-cmd "M[irror]" 'mirror-op)
(map! :leader
      :desc "Mirror project (op5)"
      "p M" #'mirror-op)

;; Gitlab issues
(defun open-issues-assigned-to-me ()
  "Return the list of open issues assigned to me."
  (projectile-with-default-dir (projectile-acquire-root)
    (s-split "\n" (shell-command-to-string "gitlab-my-issues") t)))

;; See: https://emacs.stackexchange.com/a/8177
(defun presorted-completions (completions)
  (lambda (string pred action)
    (if (eq action 'metadata)
        `(metadata (display-sort-function . ,#'identity))
      (complete-with-action action completions string pred))))

(defun pick-issue (ln)
  (interactive (list (completing-read "Issues: " (presorted-completions (open-issues-assigned-to-me)))))
  (projectile-with-default-dir (projectile-acquire-root)
    (shell-command-to-string (concat "echo '" ln "' | rg '^([0-9]*).*' --replace '$1' | BROWSER=brave-nightly xargs glab issue view -w $1"))))
(map! :leader
      "p I" #'pick-issue)

(defun open-mrs-assigned-to-me ()
  "Return the list of open mrs assigned to me."
  (projectile-with-default-dir (projectile-acquire-root)
    (s-split "\n" (shell-command-to-string "gitlab-my-mrs") t)))

(defun pick-mr (ln)
  (interactive (list (completing-read "MRs: " (presorted-completions (open-mrs-assigned-to-me)))))
  (projectile-with-default-dir (projectile-acquire-root)
    (shell-command-to-string (concat "echo '" ln "' | rg '^([0-9]*).*' --replace '$1' | BROWSER=brave-nightly xargs glab mr view -w $1"))))
(map! :leader
      "p M" #'pick-mr)
;; ### END: Project specific settings ###

;; Start the atomic chrome server
(atomic-chrome-start-server)
(setq atomic-chrome-default-major-mode 'markdown-mode)
(setq atomic-chrome-buffer-open-style 'full)
(setq atomic-chrome-extension-type-list '(ghost-text))


;; HACK: Try removing after next upgrade
;; Workaround for: https://github.com/hlissner/doom-emacs/issues/5904
;; add to $DOOMDIR/config.el
(after! lsp-mode
  (advice-remove #'lsp #'+lsp-dont-prompt-to-install-servers-maybe-a))

(setq conda-anaconda-home "/opt/anaconda")

;; RSS
(add-hook! 'elfeed-search-mode-hook #'elfeed-update)
(after! elfeed
  (setq elfeed-search-filter "@1-month-ago"))

;; External pgformatter process
(defun pgformatter-on-region ()
  "A function to invoke pgFormatter as an external program."
  (interactive)
  (let ((b (if mark-active (min (point) (mark)) (point-min)))
        (e (if mark-active (max (point) (mark)) (point-max)))
        (pgfrm "/usr/bin/pg_format --wrap-comment --comma-break --comma-end --keep-newline --wrap-limit 80 --wrap-after 1 --spaces 4" ) )
    (shell-command-on-region b e pgfrm (current-buffer) 1)) )

(defun copy-full-path-to-kill-ring ()
  "Copy buffer's full path to kill ring"
  (interactive)
  (when buffer-file-name
    (kill-new (file-truename buffer-file-name))))

;; Accept completion from copilot without interfering with lsp-mode
;; Using <left> by default for concistency with zsh completion.

(setq copilot-node-executable "/usr/bin/node")
(use-package! copilot
  :hook (prog-mode . copilot-mode)
  :bind (("C-TAB" . 'copilot-accept-completion-by-word)
         ("C-<tab>" . 'copilot-accept-completion-by-word)
         ("<left>" . 'copilot-accept-completion-by-word)
         :map copilot-completion-map
         ("C-<tab>" . 'copilot-accept-completion)
         ("C-TAB" . 'copilot-accept-completion)
         ("<left>" . 'copilot-accept-completion)))

(defun markdown-convert-buffer-to-org ()
  "Convert the current buffer's content from markdown to orgmode format and save it with the current buffer's file name but with .org extension."
  (interactive)
  (shell-command-on-region (point-min) (point-max)
                           (format "pandoc -f markdown -t org -o %s"
                                   (concat (file-name-sans-extension (buffer-file-name)) ".org"))))

;; Behavior of browse-url
(defun do-browse-url (url &rest ignore)
  "Custom browse-url function to deffer to a shell script."
  (interactive "sURL: ")
  (shell-command (concat "handle-url " url))
  ;; (kill-new url)
  (setq truncate-lines t))
(setq browse-url-browser-function 'do-browse-url)

;; ;; Basic time tracking heuristics
;; (defvar my-toggl-last-request-time nil
;;   "Timestamp of the last Toggl request.")

;; (defun my-toggl-dexlabs-project-p ()
;;   "Check if the current buffer is within a 'dexlabs' Projectile project."
;;   (and (buffer-file-name)
;;        (projectile-project-p)
;;        (string-match-p "/dexlabs/" (projectile-project-root))))

;; (defun my-toggl-start-dexlabs-timer ()
;;   "Start the Toggl timer for the 'dexlabs' project if necessary."
;;   (when (my-toggl-dexlabs-project-p)
;;     (let ((time-since-last-request (float-time (time-subtract (current-time) my-toggl-last-request-time))))
;;       (if (or (null my-toggl-last-request-time) (> time-since-last-request (* 5 60)))
;;           (progn
;;             (setq my-toggl-last-request-time (current-time))
;;             (if (zerop (shell-command "toggl now -o Dexlabs | rg -v 'no time entry'"))
;;                 (message "Toggl timer not started: there's already an active timer.")
;;               (shell-command (concat "toggl start -o Dexlabs 'Emacs activity in '$(basename " (projectile-project-root) ")"))
;;               (message "Started Toggl timer for the Dexlabs project.")))
;;         (message "Toggl timer not started: less than 5 minutes since the last request.")))))

;; (add-hook! 'after-save-hook 'my-toggl-start-dexlabs-timer)
